import Validators from './Validators';

let validator;

beforeEach(() => {
  validator = new Validators();
});

it("validates whether a field has a value", () => {
  let result = validator.required('somevalue', {
    message: 'Name is required'
  });

  expect(result).toBe(undefined);
});

it("validates whether a field has no value and returns a invalid structure", () => {
  let result = validator.required('', {
    message: 'Name is required'
  });

  expect(result.isValid).toBe(false);
});