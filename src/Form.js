import React from 'react';
import Validators from './Validators';

/**
 * Form Component
 * Receives a form schema prop which renders a form dynamically
 */
class Form extends React.Component {
  
  state = {
    formValues: {},
    formValidation: {}
  }

  /**
   * onSubmit
   * e - Form Submit event
   * Validates all fields, if valid submits the form, if invalid prevents form submission
   */
  onSubmit = (e) => {
    e.preventDefault();
    this.validateAllFields(() => {
      var isFormValid = Object.keys(this.state.formValidation).every((field) => {
        return this.state.formValidation[field] === undefined;
      });

      if(isFormValid) {
        console.log('submitting form, form data: ', this.state.formValues);
        alert('Form submitted');
      }
      else {
        alert('Form is not valid, please check values and try again');
      }
    });
  }

  /**
   * fieldChange
   * e - form input event
   * id - field id
   * Updates the state value for the corresponding field id
   * Runs validation
   */
  fieldChange = (e, id) => {
    let value = e.target.value;
    this.updateFormValue(id, value);
    let validationResult = this.validateField(id, value);
    let formValidation = Object.assign(this.state.formValidation, validationResult);
    this.setState({
      formValidation
    })
  }

  /**
   * updateFormValue
   * id - The id of the field to update
   * value - the value to update it with
   * Updates the state value for the corresponding field id
   */
  updateFormValue = (id, value) => {
    this.setState({
      formValues: {
        ...this.state.formValues,
        [id]: value
      }
    })
  }

  /**
   * validateAllFields
   * cb - callback to be invoked after form validation
   * Validates all fields at once and updates internal state representation of validation
   */
  validateAllFields = (cb) => {
    let formValidation = {};

    this.props.schema.fields.forEach((field) => {
      let fieldValidation = this.validateField(field.id, undefined, cb);
      formValidation = Object.assign(formValidation, this.state.formValidation, fieldValidation);
    });

    this.setState({
      formValidation
    }, () => cb());
  }

  // TODO: Only get 1st first
  /**
   * validateField
   * id - the id of the field to validate
   * value - the value to validate
   * Validates an individual field against its schema defined validations
   */
  validateField = (id, value) => {
    let field = this.props.schema.fields.find((ele) => {
      return ele.id === id;
    });
    let fieldValue = value || this.state.formValues[id];
    let validationObj = {};

    if (field.validations) {
      let validator = new Validators();
      Object.keys(field.validations).forEach(key => {
        let validatorFn = validator.getValidator(key);
        if (validatorFn) {
          let validationResult = validatorFn(fieldValue, field.validations[key]);
          validationObj[id] =  validationResult;
        }
      });
    }
    return validationObj;
  }
  
  /** 
   * createTextInput
   * field - The field definition object defined from the schema
   * type - Optional input type - such as text/date etc
   * Creates an individual field on the form
   */
  createTextInput = (field, type) => {
    return (
      <label>
        {field.label}
        <input type={type} onChange={(e) => this.fieldChange(e, field.id)} value={this.state.formValues[field.id] || ''} />
        {
          this.state.formValidation[field.id] && 
            <div>{this.state.formValidation[field.id].message}</div>
        }
      </label>
    )
  }

  /**
   * generateFields fields
   * fields - The collection of fields to create
   * Accepts a collection of fields and generates them
   */
  generateFields = (fields) => {
    return fields.map((field, key) => {
      return this.generateField(field, key);
    })
  }

  /**
   * generateField
   * field - The schema definition of a field
   * key - The key as to make this item distinct as it will be in a collection
   * Generates a field based on schema definition
   */
  generateField = (field, key) => {
    let fieldInput;
    switch (field.type) {
      case 'text':
      case 'date':
        fieldInput = this.createTextInput(field, field.type);
        break;
      default:
        break;
    }
    return (
      <div key={key}>
        {fieldInput}
      </div>
    )
  }
  
  render() {
    const schema = this.props.schema;
    const fields = this.generateFields(schema.fields);

    return (
      <form onSubmit={this.onSubmit}>
        <h1>{schema.formTitle}</h1>
        {
          fields
        }
        <button type="submit">Submit</button>
        <p>Form Values:</p>
      <p>{
        JSON.stringify(this.state.formValues) 
      }</p>
      <p>Form Validations:</p>
      <p>{
        JSON.stringify(this.state.formValidation) 
      }</p>
      </form>

    )
  }
}

export default Form;
