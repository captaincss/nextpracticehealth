import React from 'react';
import ReactDOM from 'react-dom';
import Form from './Form';

it('renders without crashing', () => {
  const formSchema = {
    formTitle: 'Person Details',
    fields: [
      {
        id: 'name',
        type: 'text',
        label: 'Name',
        validations: {
          required: {
            message: 'Name is required'
          },
          regex: {
            rule: /[a-z]+\s[a-z]+/,
            message: 'Name must be two words separated by a space - e.g. John Smith'
          }
        }
      },
      {
        id: 'age',
        type: 'date',
        label: 'Age',
        validations: {
          required: {
            message: 'Age is required'
          },
          minAge: {
            min: 18,
            message: 'You must be at least 18'
          }
        }
      }
    ]
  };
  const div = document.createElement('div');
  ReactDOM.render(<Form schema={formSchema} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
