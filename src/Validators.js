
/**
 * Validators
 * Class which handles field level validation based on whats defined in form schema for that field 
 **/
class Validators {
  /**
   * getValidator
   * type - type of validator to return
   * Returns corresponding validator based on type
   */
  getValidator = (type) => {
    switch (type) {
      case 'required':
        return this.required;

      case 'regex':
        return this.regex;

      case 'minAge':
        return this.minAge;
    
      default:
        break;
    }
  }

  /**
   * required
   * value - value to validate
   * validation - validation object defined from schema definition
   * Checks whether a field has any value
   */
  required = (value, validation) => {
    let val = value && value.trim();

    if (!val) {
      return {
        isValid: false,
        message: validation.message || 'This field is required'
      }
    }
    return undefined;
  }

  /**
   * regex
   * value - value to validate
   * validation - validation object defined from schema definition
   * Validated field value against regex defined in schema
   */
  regex = (value, validation) => {
    let val = value && value.trim();
    let regex = new RegExp(validation.rule, 'i');

    if (!regex.test(val)) {
      return {
        isValid: false,
        message: validation.message || 'Invalid format'
      }
    }
    return undefined;
  }

  /**
   * minAge
   * value - value to validate
   * validation - validation object defined from schema definition
   * Validates a date to ensure minimum age
   */
  minAge = (value, validation) => {
    let val = value && value.trim();

    // if (!val) {
    //   return undefined;
    // }
    val = new Date(val);
    let minAge = validation.min;
    let ageDifMs = Date.now() - val.getTime();
    let ageDate = new Date(ageDifMs); // miliseconds from epoch
    let age =  Math.abs(ageDate.getUTCFullYear() - 1970);

    if(!value || age < minAge) {
      return {
        isValid: false,
        message: validation.message || 'Less than minimum age'
      }
    }
    return undefined;
  }

}

export default Validators;